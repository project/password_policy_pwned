<?php

namespace Drupal\password_policy_pwned\Plugin\PasswordConstraint;

@trigger_error('The ' . __NAMESPACE__ . '\PasswordPnwed class is deprecated in password_policy_pwned:2.0.0 and is removed from password_policy_pwned:3.0.0. Use \Drupal\password_policy_pwned\Plugin\PasswordConstraint\PasswordPwned instead. See https://www.drupal.org/node/3434839', E_USER_DEPRECATED);

// cspell:ignore pnwed

/**
 * Enforces that a password was not exposed through data breaches.
 *
 * @deprecated in password_policy_pwned:2.0.0 and is removed from
 *   password_policy_pwned:3.0.0. Use
 *   \Drupal\password_policy_pwned\Plugin\PasswordConstraint\PasswordPwned
 *   instead.
 *
 * @see https://www.drupal.org/node/3434839
 */
class PasswordPnwed extends PasswordPwned {

}
